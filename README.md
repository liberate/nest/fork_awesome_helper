Helper for Fork Awesome icons
=========================================

A helper for ruby to easily include Fork Awesome SVG icons.

This help only supports inline SVG. Why inline icons instead of font-based?

* Inline icons are faster (as long as you don't have dozens of icons on a single page).
* Load only the icons you use.
* You can easily add new icons without building a new font.

Installation
-----------------------------------------

Add this to your `Gemfile`

    gem 'fork_awesome_helper'

Usage
-----------------------------------------

In your views

    <%= icon 'check'                                    %>
    <%= icon 'home', fill: 'green', class: 'myicon'     %>
    <%= icon 'bomb', width: 100                         %>
    <%= icon 'square', "Label", height: 10              %>
    <%= icon('square', "aria-label": "label") { "Sqr" } %>

Configuration Options
-----------------------------------------

    ForkAwesome.config do |config|
      config.raise_exception = false  # if true, raise an exception if the icon name is not found
      config.width = 22               # default width
      config.height = 22              # default height
      config.fill = 'black'           # default fill color
      config.default = 'square'       # default if icon is not found (not compatible with raise_exception=true)
      config.paths << 'my/additional/custom/icons/path'
    end

See Also
-----------------------------------------

Inspired by these inline SVG icon gems:

* https://github.com/marcelolx/bootstrap-icons
* https://github.com/primer/octicons