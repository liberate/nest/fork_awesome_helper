require 'active_support'
require 'active_support/core_ext/string'
require "fork_awesome/config"
require "fork_awesome/helper"
require "fork_awesome/icon"

module ForkAwesomeHelper
  if defined?(Rails)
    class Railtie < ::Rails::Railtie
      initializer "fork_awesome_helper.helper" do
        ActionView::Base.send :include, ForkAwesome::Helper
      end
    end
  end
end
