module ForkAwesome
  class Icon
    attr_reader :symbol, :path, :width, :height

    def initialize(symbol)
      @symbol = symbol.to_s
      @width, @height, @path = parse_icon_data(load_icon_file(@symbol))
    end

    def to_svg(options={})
      options = evaluate_options(options)
      "<svg #{html_attributes(options)}>#{path}</svg>"
    end

    private

    def html_attributes(options)
      options.map {|attr, value| '%s="%s"' % [attr, value]}.join(' ')
    end

    def find_path(symbol)
      ForkAwesome.config.paths.each do |path|
        path = File.join(path, symbol + '.svg')
        return path if File.exist?(path)
      end
      return false
    end

    def load_icon_file(symbol)
      icon_path = find_path(symbol)
      if icon_path
        File.read(icon_path)
      elsif ForkAwesome.config.raise_exception
        raise StandardError, "The icon '%s' does not exist." % symbol
      else
        File.read(find_path(ForkAwesome.config.default))
      end
    end

    #
    # a crude regexp parsing of the raw svg string.
    #
    def parse_icon_data(svg_string)
      width  = svg_string.match(/width=\"(\d+)\"/)&.[](1)
      height = svg_string.match(/height=\"(\d+)\"/)&.[](1)
      path   = svg_string.match(/(<path .+?>)/)&.[](1)
      return [width, height, path]
    end

    def evaluate_options(options)
      opts = options.nil? ? {} : options.dup
      opts[:class]  = "fa fa-#{symbol} #{opts[:class]}".strip
      opts[:height] ||= opts[:width] || ForkAwesome.config.height
      opts[:width]  ||= opts[:height] || ForkAwesome.config.width
      opts[:fill]   ||= ForkAwesome.config.fill
      opts[:viewBox] = "0 0 #{width} #{height}"
      if opts["aria-label"].nil? && opts[:"aria-label"].nil?
        opts["aria-hidden"] = true
      else
        opts[:role] = "img"
      end
      opts
    end
  end
end
