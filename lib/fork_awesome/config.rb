module ForkAwesome
  def self.config
    @@config ||= ForkAwesome::Config.new
    yield(@@config) if block_given?
    @@config
  end

  class Config
    attr_accessor :raise_exception, :width, :height, :fill, :default, :paths

    def initialize
      @raise_exception = false
      @width = 22
      @height = 22
      @fill = 'black'
      @default = 'square'
      @paths = [File.expand_path("../../../icons", __FILE__)]
    end
  end
end
